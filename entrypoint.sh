#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z logs-db 5432; do
  sleep 0.1
done

echo "PostgreSQL started"

# python scripts/store_logs.py
python scripts/count_visitors.py
python scripts/count_browsers.py
python scripts/count_pages.py
python scripts/count_referrers.py

# Uses rate limited API
# python scripts/count_countries.py
