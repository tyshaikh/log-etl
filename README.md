# Access Log ETL

A series of scripts to conduct ETL on raw access logs from Apache and Nginx servers.

## Data Pipeline

![Pipeline](data-flow.png)

Each pipeline component is separated from the others, takes in a defined input and returns a defined output.

The basic structure is as follows:

1. Use raw logs from an Apache or Nginx server
2. Store the raw data in a relational database in case different analysis needs to be run
3. Remove duplicate records from visitors doing the same action
4. Modularize functionality into separate components.
