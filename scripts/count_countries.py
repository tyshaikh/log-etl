import os
import pickle
import ipinfo
from datetime import datetime
from helpers import get_db_conn, upload_file


ACCESS_TOKEN = os.getenv('IP_ACCESS_TOKEN')


def get_lines(time_obj):
    "Query any rows after the given timestamp"
    conn = get_db_conn()
    cur = conn.cursor()
    cur.execute("SELECT remote_addr, time_local FROM logs WHERE created > %s LIMIT 500", [time_obj])
    resp = cur.fetchall()
    cur.close()
    conn.close()
    return resp


def get_time_and_country(lines):
    "Extract the country and time from each row"
    countries = []
    times = []
    for line in lines:
        countries.append(parse_ip(line[0]))
        times.append(parse_time(line[1]))
    return countries, times


def parse_time(time_str):
    "Parse time from a string into datetime"
    try:
        time_obj = datetime.strptime(time_str, '[%d/%b/%Y:%H:%M:%S %z]')
    except Exception:
        time_obj = ""
    return time_obj


def parse_ip(ip_address):
    "Identify which country the IP address belongs to"
    handler = ipinfo.getHandler(ACCESS_TOKEN)

    try:
        details = handler.getDetails(ip_address)
        country_name = details.country_name
        return country_name
    except:
        return "Other"


if __name__ == "__main__":
    country_counts = {}
    start_time = datetime(year=2013, month=1, day=1)

    lines = get_lines(start_time)
    countries, times = get_time_and_country(lines)

    if len(times) > 0:
        start_time = times[-1]

    for country, time_obj in zip(countries, times):
        if country not in country_counts:
            country_counts[country] = 0
        country_counts[country] += 1

    count_list = country_counts.items()
    count_list = sorted(count_list, key=lambda x: x[1], reverse=True)

    print("")
    print(datetime.now())
    for item in count_list:
        print("{}: {}".format(*item))

    filename = "countries_count.p"
    path = "./data/processed/"
    pickle.dump(count_list, open((path+filename), "wb"))
    upload_file(path, filename)
