import os
import pickle
from urllib.parse import urlparse
import psycopg2
from datetime import datetime


result = urlparse(os.environ.get("DATABASE_URL"))
username = result.username
password = result.password
database = result.path[1:]
hostname = result.hostname
LOG_FILE = "../data/split/sample.log"


def get_db_conn():
    "Return database connection"
    try:
        conn = psycopg2.connect(database=database,
                                user=username,
                                password=password,
                                host=hostname)
        return conn
    except Exception:
        print("Unable to connect to the database")
        return None


def create_table():
    "Create initial logs table"
    conn = get_db_conn()
    cur = conn.cursor()

    cur.execute("""
    CREATE TABLE IF NOT EXISTS logs (
      raw_log TEXT NOT NULL UNIQUE,
      remote_addr TEXT,
      time_local TEXT,
      request_type TEXT,
      request_path TEXT,
      status INTEGER,
      body_bytes_sent INTEGER,
      http_referer TEXT,
      http_user_agent TEXT,
      created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
      )
    """)
    conn.commit()

    cur.close()
    conn.close()


def parse_line(line):
    "Parse raw text of log file line"
    split_line = line.split(" ")
    if len(split_line) < 12:
        return []
    remote_addr = split_line[0]
    time_local = split_line[3] + " " + split_line[4]
    request_type = split_line[5]
    request_path = split_line[6]
    status = split_line[8]
    body_bytes_sent = split_line[9]
    http_referer = split_line[10]
    http_user_agent = " ".join(split_line[11:])
    created = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

    return [
        remote_addr,
        time_local,
        request_type,
        request_path,
        status,
        body_bytes_sent,
        http_referer,
        http_user_agent,
        created
    ]


def insert_record(line, parsed):
    "Insert line from log file into database"
    conn = get_db_conn()
    cur = conn.cursor()
    try:
        cur.execute("""
            INSERT INTO logs VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """, (line, parsed[0], parsed[1], parsed[2], parsed[3], parsed[4], parsed[5], parsed[6], parsed[7], parsed[8]))
    except psycopg2.Error or psycopg2.IntegrityError or Exception:
        conn.rollback()
    else:
        conn.commit()

    cur.close()
    conn.close()


if __name__ == '__main__':
    create_table()
    file = open(LOG_FILE, 'r')
    lines = file.readlines()

    for line in lines:
        line = line.strip()
        parsed = parse_line(line)
        if len(parsed) > 0:
            insert_record(line, parsed)

    file.close()
