import pickle
from datetime import datetime
from helpers import get_db_conn, upload_file


def get_lines(time_obj):
    "Query any rows after the given timestamp"
    conn = get_db_conn()
    cur = conn.cursor()
    cur.execute("SELECT time_local, request_path FROM logs WHERE created > %s", [time_obj])
    resp = cur.fetchall()
    cur.close()
    conn.close()
    return resp


def get_time_and_path(lines):
    "Extract the time and path from each row"
    times = []
    paths = []
    for line in lines:
        times.append(parse_time(line[0]))
        paths.append(line[1])
    return times, paths


def parse_time(time_str):
    "Parse time from a string into datetime"
    try:
        time_obj = datetime.strptime(time_str, '[%d/%b/%Y:%H:%M:%S %z]')
    except Exception:
        time_obj = ""
    return time_obj


if __name__ == "__main__":
    path_counts = {}
    start_time = datetime(year=2013, month=1, day=1)

    lines = get_lines(start_time)
    times, paths = get_time_and_path(lines)

    if len(times) > 0:
        start_time = times[-1]

    for path, time_obj in zip(paths, times):
        if path not in path_counts:
            path_counts[path] = 0
        path_counts[path] += 1

    count_list = path_counts.items()
    count_list = sorted(count_list, key=lambda x: x[1], reverse=True)
    top_pages = count_list[:30]

    print("")
    print(datetime.now())
    for item in top_pages:
        print("{}: {}".format(*item))

    filename = "pages_count.p"
    path = "./data/processed/"
    pickle.dump(count_list, open((path+filename), "wb"))
    upload_file(path, filename)
