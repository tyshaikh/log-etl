import pickle
from datetime import datetime
from helpers import get_db_conn, upload_file


def get_lines(time_obj):
    "Query any rows after the given timestamp"
    conn = get_db_conn()
    cur = conn.cursor()
    cur.execute("SELECT time_local, http_referer FROM logs WHERE created > %s", [time_obj])
    resp = cur.fetchall()
    cur.close()
    conn.close()
    return resp


def get_time_and_referrer(lines):
    "Extract the time and referrer from each row"
    times = []
    referrers = []
    for line in lines:
        times.append(parse_time(line[0]))
        referrers.append(line[1])
    return times, referrers


def parse_time(time_str):
    "Parse time from a string into datetime"
    try:
        time_obj = datetime.strptime(time_str, '[%d/%b/%Y:%H:%M:%S %z]')
    except Exception:
        time_obj = ""
    return time_obj


if __name__ == "__main__":
    referrer_counts = {}
    start_time = datetime(year=2013, month=1, day=1)

    lines = get_lines(start_time)
    times, referrers = get_time_and_referrer(lines)

    if len(times) > 0:
        start_time = times[-1]

    for referrer, time_obj in zip(referrers, times):
        if referrer not in referrer_counts:
            referrer_counts[referrer] = 0
        referrer_counts[referrer] += 1

    count_list = referrer_counts.items()
    count_list = sorted(count_list, key=lambda x: x[1], reverse=True)
    top_referrers = count_list[:30]

    print("")
    print(datetime.now())
    for item in top_referrers:
        print("{}: {}".format(*item))


    filename = "referrers_count.p"
    path = "./data/processed/"
    pickle.dump(count_list, open((path+filename), "wb"))
    upload_file(path, filename)
