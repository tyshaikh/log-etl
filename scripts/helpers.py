import os
import boto3
from urllib.parse import urlparse
import psycopg2


result = urlparse(os.getenv("DATABASE_URL"))
username = result.username
password = result.password
database = result.path[1:]
hostname = result.hostname


ACCESS_KEY = os.getenv('AWS_ACCESS_KEY')
SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')
BUCKET_NAME = os.getenv('AWS_S3_BUCKET')


def get_db_conn():
    "Return database connection"
    try:
        conn = psycopg2.connect(database=database,
                                user=username,
                                password=password,
                                host=hostname)
        return conn
    except Exception:
        print("Unable to connect to the database")
        return None


def upload_file(path, filename):
    "Upload single file to S3 bucket"
    S3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_ACCESS_KEY)

    FILE_NAME = path + filename
    OBJECT_NAME = filename

    S3.upload_file(FILE_NAME, BUCKET_NAME, OBJECT_NAME)


def download_file(file):
    "Download single file from S3 bucket"
    S3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_ACCESS_KEY)

    OBJECT_NAME = file
    FILE_NAME = file

    S3.download_file(BUCKET_NAME, OBJECT_NAME, FILE_NAME)
