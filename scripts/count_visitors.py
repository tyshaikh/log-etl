import pickle
from datetime import datetime
from helpers import get_db_conn, upload_file


def get_lines(time_obj):
    "Query any rows after the given timestamp"
    conn = get_db_conn()
    cur = conn.cursor()
    cur.execute("SELECT remote_addr, time_local FROM logs WHERE created > %s", [time_obj])
    resp = cur.fetchall()
    cur.close()
    conn.close()
    return resp


def get_time_and_ip(lines):
    "Extract the IP address and time from each row"
    ips = []
    times = []
    for line in lines:
        ips.append(line[0])
        times.append(parse_time(line[1]))
    return ips, times


def parse_time(time_str):
    "Parse time from a string into datetime"
    try:
        time_obj = datetime.strptime(time_str, '[%d/%b/%Y:%H:%M:%S %z]')
    except Exception:
        time_obj = ""
    return time_obj


if __name__ == "__main__":
    unique_ips = {}
    counts = {}
    start_time = datetime(year=2013, month=1, day=1)

    lines = get_lines(start_time)
    ips, times = get_time_and_ip(lines)

    if len(times) > 0:
        start_time = times[-1]

    for ip, time_obj in zip(ips, times):
        day = time_obj.strftime("%d-%m-%Y")
        if day not in unique_ips:
            unique_ips[day] = set()
        unique_ips[day].add(ip)

    for k, v in unique_ips.items():
        counts[k] = len(v)

    count_list = counts.items()
    count_list = sorted(count_list, key=lambda x: x[0])

    print("")
    print(datetime.now())
    for item in count_list:
        print("{}: {}".format(*item))

    filename = "visitors_count.p"
    path = "./data/processed/"
    pickle.dump(count_list, open((path+filename), "wb"))
    upload_file(path, filename)
