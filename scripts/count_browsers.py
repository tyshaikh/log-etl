import pickle
from datetime import datetime
from helpers import get_db_conn, upload_file


def get_lines(time_obj):
    "Query any rows after the given timestamp"
    conn = get_db_conn()
    cur = conn.cursor()
    cur.execute("SELECT time_local, http_user_agent FROM logs WHERE created > %s", [time_obj])
    resp = cur.fetchall()
    cur.close()
    conn.close()
    return resp


def get_time_and_browser(lines):
    "Extract the time and browser from each row"
    times = []
    browsers = []
    for line in lines:
        times.append(parse_time(line[0]))
        browsers.append(parse_user_agent(line[1]))
    return times, browsers


def parse_time(time_str):
    "Parse time from a string into datetime"
    try:
        time_obj = datetime.strptime(time_str, '[%d/%b/%Y:%H:%M:%S %z]')
    except Exception:
        time_obj = ""
    return time_obj


def parse_user_agent(user_agent):
    "Identify which browser is in user agent string"
    browsers = ["Firefox", "Chrome", "Opera", "Safari", "MSIE"]
    for browser in browsers:
        if browser in user_agent:
            return browser
    return "Other"


if __name__ == "__main__":
    browser_counts = {}
    start_time = datetime(year=2013, month=1, day=1)

    lines = get_lines(start_time)
    times, browsers = get_time_and_browser(lines)

    if len(times) > 0:
        start_time = times[-1]

    for browser, time_obj in zip(browsers, times):
        if browser not in browser_counts:
            browser_counts[browser] = 0
        browser_counts[browser] += 1

    count_list = browser_counts.items()
    count_list = sorted(count_list, key=lambda x: x[0])

    print("")
    print(datetime.now())
    for item in count_list:
        print("{}: {}".format(*item))

    filename = "browsers_count.p"
    path = "./data/processed/"
    pickle.dump(count_list, open((path+filename), "wb"))
    upload_file(path, filename)
