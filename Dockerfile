FROM python:3.7.4-alpine

RUN apk update && \
    apk add --virtual build-deps gcc python-dev musl-dev && \
    apk add postgresql-dev && \
    apk add netcat-openbsd

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/etl

RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/etl/requirements.txt
RUN pip install -r requirements.txt

COPY ./entrypoint.sh /usr/src/etl/entrypoint.sh
RUN chmod +x /usr/src/etl/entrypoint.sh

COPY . /usr/src/etl/
